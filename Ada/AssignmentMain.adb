with Ada.Text_IO;             use Ada.Text_IO;
with MatrixMult;              use MatrixMult;  
With Ada.Integer_Text_Io;     use Ada.Integer_Text_Io; --this allows you to use get int and put int              

procedure AssignmentMain is
  
  package int_io is new integer_io(integer);--this is what allows you to use get and put.     
  x : integer;	
  MatrixA : matrix;
  MatrixB : matrix;
  MatrixC : matrix;

TASK Reader1 is 
  Entry Start;
end Reader1;

TASK Reader2 is 
  Entry Start;
end Reader2;

TASK Printer is 
  Entry Start;
end Printer;



TASK BODY Reader1 is

  begin
  accept Start do

  for Row in 1 .. SIZE--ctwo embeded loops here
   loop
     for Column in 1 .. SIZE 
     loop
     get(x);
     MatrixA(Row, Column) := x;
     end loop;
    end loop; 
    
end Start;
end Reader1;  


TASK BODY Reader2 is

  begin
  accept Start do

  for Row in 1 .. SIZE--ctwo embeded loops here
   loop
     for Column in 1 .. SIZE 
     loop
     get(x);
     MatrixB(Row, Column) := x;
     end loop;
    end loop; 
    
end Start;
end Reader2;  


TASK BODY Printer is

  begin
  accept Start do

   for Row in 1 .. SIZE--ctwo embeded loops here
   loop
     for Column in 1 .. SIZE 
     loop
     put(MatrixC(Row, Column));
     end loop;
     new_line(1);
    end loop;   
 end Start;
 end Printer;  


begin

Reader1.start;
Reader2.start;
MatMult( MatrixA, MAtrixB , MAtrixC);
Printer.start;

end AssignmentMain;
 
