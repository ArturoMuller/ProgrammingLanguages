--matrixmult.adb
with Ada.Text_IO;             use Ada.Text_IO; -- delete later
With Ada.Integer_Text_Io;     use Ada.Integer_Text_Io;
package body matrixmult is



   
    procedure MatMult( A : in matrix; B : in matrix; C: in out matrix) is
    sum : integer;
    begin 
	   
      
   
      for Column in 1 .. SIZE
      loop
        for MRow in 1 .. SIZE 
        loop
            sum := 0;
            for MColumn in 1 .. SIZE 
            loop
             sum :=  sum + A(Column, MColumn) * B(MColumn, MRow); 
            end loop;
        C(Column, MRow) := sum;
                   
        end loop;   
       
       end loop;


    
       
    end MatMult;
end matrixmult;