Control.Print.printDepth := 100;
Control.Print.printLength := 100;


(*1.   intBubble*)

(*final*)
fun intBubble(nil) = nil
|   intBubble(x :: nil) = [x]
|   intBubble(x :: y :: xs) = 
	if x > y then intBubble(y :: xs) @ [x]
	else intBubble( x :: xs) @ [y]


(*2. intBubbleSort*)

	fun intBubbleSort(nil) = nil
|   intBubbleSort(x :: nil) = [x]
|   intBubbleSort(x :: xs) = 
	let
		val (x :: xs) = intBubble(x :: xs)

	in
		
		[x] @ intBubbleSort(xs)
	end
	

(*3.  bubbleSort *)
fun bubble (op <) (nil) = nil
|   bubble (op <) (x :: nil) = [x]
|   bubble (op <) (x :: y :: xs) = 
	if (x < y) then (bubble (op <) (x :: xs)) @ [y]
	else (bubble (op <) ( y :: xs)) @ [x]


fun bubbleSort (op <) (nil) = nil
|   bubbleSort (op <) (x :: nil) = [x]
|   bubbleSort (op <) (x :: xs) = 
	let
		val (x :: xs) = bubble (op <) (x :: xs)

	in
		
		[x] @ (bubbleSort (op <) (xs))
	end





(*4 tree data type*)

datatype 'a tree = leaf of 'a | node of 'a tree list


(*5 height*)

fun maxList [] = 0
|	maxList [x] = x
|   	maxList(x :: y :: xs) = 
		if x > y then maxList(x :: xs)
		else maxList(y :: xs)



fun height(leaf x) = 0
|	height(node l) = 1 + maxList((map height) l)







(*6 polymorphic sort tree*)

fun sortTree (op <) (leaf x) = leaf (bubbleSort (op <) x)
|	sortTree (op <) (node l) = node ((map (sortTree (op <)) l))

(*sortTree (op <) (node [leaf [4,2,3,1], leaf [7,2,5,0]])*)
(*7 flatten tree*)

fun flattenTree (leaf x) = [x]
|	flattenTree (node l) = foldr (op @) nil (map (flattenTree) l)


(*8 polymorphic merge *)

fun   merge (op <) [] l = l 
|     merge (op <) l [] = l		
|     merge (op <) (x :: xs) (y :: ys) = if (x < y) then x :: (merge (op <) xs (y :: ys)) else y :: (merge (op <) (x :: xs) ys)
	

(*9 mergeTree *)	

fun mergeTree (op <) (node l) = 
   let 
		val (x :: xs) = (flattenTree (sortTree (op <) (node l)))
	in
		 foldr (fn(x,y)=> merge (op <) x y) x  xs
	end
