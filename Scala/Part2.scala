class A(x: Int) extends Ordered[A]
{
  val value = x
  val comparisonvalue = x
  def compare(other: A): Int =  this.comparisonvalue  - other.comparisonvalue
  override def toString() = "A<" + value + ">"  
}
  
   
 class B(x: Int,y: Int) extends A(x)
{
  val othervalue = y
  override val comparisonvalue = x + y
  override def toString() = "B<" + value + ","+ othervalue +  ">"  
}
 
 class C[+T](g: Int => T)
{
  def function = g
  def apply(x : Int): T = 
   {
   function(x);
   }	   
}
  

abstract class Tree[T <: Ordered[T]]
{
  def insert(x: T):Tree[T]
  
}


 case class Empty[T <: Ordered[T]]() extends Tree[T]
{
  override def toString() = "" 
  override def insert(x : T): Tree[T] = {
  new Leaf(x)
  } 
}
 
case class Leaf[T <: Ordered[T]](v : T) extends Tree[T]
{
  override def toString() = "" + v + "" 
  override def insert(x : T): Tree[T] = {
    if(x > v)
  {
   new Node(new Leaf(v) , x , new Empty())
    }
  else 
  {
  new Node( new Empty()  , x , new Leaf(v) )
  }
} 
}

  case class Node[T <: Ordered[T]](l : Tree[T] , v: T , r : Tree[T]) extends Tree[T]
{
  override def toString() = "(" + l  + "," + v + "," + r +")"
  override def insert(x : T): Tree[T] = 
  {
  if(x > v)
  {
  new Node(l, v, r.insert(x))
  }
  else 
  {
  new Node(l.insert(x) , v, r)
  }
   }  
}
 
 
object Part2
{

def applyTo10(n : C[A]) =
{
  n.apply(10);
}

def main(args: Array[String]) {
test()
}

def test() {
    val c1 = new C((x:Int) => new A(x))
    println(c1.apply(3))

    val c2 = new C((x:Int) => new B(x+1, x+2))
    println(c2.apply(3))

    println(applyTo10(c1))
    println(applyTo10(c2))  //relies on covariant subtyping

    var t1: Tree[A] = Empty()

    t1 = t1.insert(new A(4))
    t1 = t1.insert(new A(3))
    t1 = t1.insert(new B(4,1))
    t1 = t1.insert(new A(2))
    println(t1)
  }
} 
 
